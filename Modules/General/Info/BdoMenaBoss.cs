﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Eshirixis.Common.Attributes;
using Eshirixis.Common.Enums;
using Eshirixis.Common.Extensions;
using Eshirixis.Common.Models;
using Eshirixis.Services;

namespace Eshirixis.Modules.General
{
    [Name("bdo"), Summary("contains the bdo boss info commands.")]
    public partial class BdoMenaBoss : ModuleBase<SocketCommandContext>
    {
        [Group("bosstimer"), Name("Bdo Boss Timer")]
        public class InfoBdoSub : ModuleBase<SocketCommandContext>
        {
            [Command("")]
            [Remarks("Sets BDO MENA boss timer here.")]
            [Priority(0)]
            public async Task SetBDOTimer()
            {
                using (var db = new NeoContext())
                {
                    if (db.BdoReminders.Any(t =>
                        t.Guild == db.Guilds.FirstOrDefault(g => g.Id == Context.Guild.Id)))
                    {
                        var embed2 = NeoEmbeds.Minimal("Boss Reminder deleted.");
                        await ReplyAsync("", false, embed2.Build());
                        db.BdoReminders.Remove(db.BdoReminders.First(b => b.Guild == db.Guilds.FirstOrDefault(g => g.Id == Context.Guild.Id)));
                        return;
                    }

                    var u = new Bdo
                    {
                        Guild = db.Guilds.First(x => x.Id == Context.Guild.Id),
                        ChannelID = Context.Channel.Id
                    };
                    db.BdoReminders.Add(u);
                    db.SaveChanges();
                    var embed = NeoEmbeds.Minimal("Boss Reminder added to this channel.");
                    await ReplyAsync("", false, embed.Build());
                }                
            }
        }        
    }
}
