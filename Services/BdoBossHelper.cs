﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eshirixis.Services
{
    public static class BdoBossHelper
    {
        public static List<Tuple<DayOfWeek, string, string>> bossArray = new List<Tuple<DayOfWeek, string, string>>();

        public static void BdoBossInitiate()
        {
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Monday, "00:45", "Karanda"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Monday, "10:45", "Nouver"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Monday, "15:45", "Kutum/Kzarka"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Monday, "19:45", "Nouver"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Monday, "22:45", "Kzarka"));
            //end of day  new                   
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Tuesday, "00:45", "Kutum"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Tuesday, "10:45", "Kzarka"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Tuesday, "15:45", "Nouver/Kutum"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Tuesday, "19:45", "Kzarka/Karanda"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Tuesday, "22:45", "Nouver"));
            //end of day  new                    
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Wednesday, "00:45", "Kzarka"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Wednesday, "10:45", "Kutum"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Wednesday, "15:45", "Nouver/Kzarka"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Wednesday, "19:45", "Nouver/Kutum"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Wednesday, "22:45", "Karanda"));
            //end of day lnew ast                
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Thursday, "00:45", "Karanda"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Thursday, "10:45", "Nouver"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Thursday, "15:45", "Kutum/Kzarka"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Thursday, "19:45", "Nouver/Kutum"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Thursday, "22:45", "Karanda/Kzarka"));
            //end of day  new                    
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Friday, "00:45", "Karanda"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Friday, "10:45", "Nouver"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Friday, "15:45", "Kutum/Kzarka"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Friday, "19:45", "Nouver/Kzarka"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Friday, "22:45", "Kutum"));
            //end of day  new                    
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Saturday, "00:45", "Kzarka"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Saturday, "10:45", "Kutum/Kzarka"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Saturday, "15:45", "Kutum/Nouver"));
            //EMPTY BOSS AREA                                                      "19:45",     
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Saturday, "22:45", "Karanda/Nouver"));
            //end of day  new                      
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Sunday, "00:45", "Nouver"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Sunday, "10:45", "Kutum"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Sunday, "15:45", "Karanda/Kzarka"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Sunday, "19:45", "Nouver/Karanda"));
            bossArray.Add(new Tuple<DayOfWeek, string, string>(DayOfWeek.Sunday, "22:45", "Kzarka/Kutum"));
            //end of day
        }
    }
}
