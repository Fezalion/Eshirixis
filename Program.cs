using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime;
using Discord;
using Discord.Addons.Interactive;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using Eshirixis.Common.Models;
using Eshirixis.Common.Types;
using Eshirixis.Common.Utility;
using Eshirixis.Common.Extensions;
using Eshirixis.Services;
using Eshirixis.Modules.General;

namespace Eshirixis
{
    internal class Program
    {
        private static void Main() => new Program().Run().GetAwaiter().GetResult();

        private DiscordSocketClient _client;
        private IServiceProvider _services;
        private Timer _playingTimer = null;
        private Timer _BdoBoss = null;
        private AutoResetEvent _autoEvent = null;
        private AutoResetEvent _autoEvent2 = null;

        private static async Task PrintInfoAsync()
        {
            var art = new[] {
                @"          ███████╗███████╗██╗  ██╗██╗██████╗ ██╗██╗  ██╗██╗███████╗",
                @"          ██╔════╝██╔════╝██║  ██║██║██╔══██╗██║╚██╗██╔╝██║██╔════╝",
                @"          █████╗  ███████╗███████║██║██████╔╝██║ ╚███╔╝ ██║███████╗",
                @"          ██╔══╝  ╚════██║██╔══██║██║██╔══██╗██║ ██╔██╗ ██║╚════██║",
                @"          ███████╗███████║██║  ██║██║██║  ██║██║██╔╝ ██╗██║███████║",
                @"          ╚══════╝╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═╝╚═╝╚══════╝"
            };
            foreach (var line in art)
                await NeoConsole.NewLineArt(line, ConsoleColor.DarkMagenta);
            await NeoConsole.Append("\n+-------------------------------------------------------------------------+", ConsoleColor.Gray);
            await NeoConsole.Append("\n   Source Code(Github Repo): https://github.com/EchelonDev/NeoKapcho/      ", ConsoleColor.Yellow);
            await NeoConsole.Append("\n         Build with love by 48 | Powered by SQLite and EFCore              ", ConsoleColor.Red);
            await NeoConsole.Append("\n+-------------------------------------------------------------------------+", ConsoleColor.Gray);
        }
        private async Task Run()
        {
            Console.OutputEncoding = Encoding.Unicode;
            await PrintInfoAsync();
            using (var db = new NeoContext())
            {
                db.Database.EnsureCreated();
            }
            EnsureConfigExists();
            //BdoBossHelper.BdoBossInitiate();

            _client = new DiscordSocketClient(new DiscordSocketConfig
            {
                LogLevel = LogSeverity.Info,
                MessageCacheSize = 5000
            });

            await _client.LoginAsync(TokenType.Bot, Configuration.Load().Token);
            await NeoConsole.Append("Logging in...", ConsoleColor.Cyan);
            await _client.StartAsync();
            await NeoConsole.NewLine("Success...", ConsoleColor.Cyan);

            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton<InteractiveService>()
                .AddSingleton<CommandHandler>()
                .BuildServiceProvider();
            await NeoConsole.NewLine("Activating Services...\nDone.", ConsoleColor.Cyan);

            await _services.GetRequiredService<CommandHandler>().Install(_client, _services);
            await NeoConsole.NewLine("Command Handler starting...\nDone.\n", ConsoleColor.Cyan);

            _client.Log += Log;
            _client.Ready += _client_Ready;

            await Task.Delay(-1);
        }

        private Task _client_Ready()
        {
            Task.Run(() =>
            {
                foreach (var guild in _client.Guilds)
                {
                    using (var db = new NeoContext())
                    {
                        if (db.Guilds.Any(x => x.Id == guild.Id)) continue;
                        var newobj = new Guild
                        {
                            Id = guild.Id,
                            Prefix = null
                        };
                        db.Guilds.Add(newobj);
                        db.SaveChanges();
                        Console.WriteLine($"Guild Added : {guild.Name}");
                    }
                }
            });

            Task.Run(async () =>
            {
                using (var db = new NeoContext())
                {
                    foreach (var hub in db.NeoHubSettings)
                    {
                        var m = await (_client.GetChannel(hub.ChannelId) as ITextChannel)
                            .GetMessageAsync(hub.MsgId);
                        await (m as IUserMessage).ModifyAsync(x => x.Embed = NeoEmbeds.Information(_client));
                    }
                }
            });

            _autoEvent2 = new AutoResetEvent(false);
            _BdoBoss = new Timer(RemindBdoBoss, _autoEvent, 0, 1000 * 59);

            _autoEvent = new AutoResetEvent(false);
            _playingTimer = new Timer(ChangePlaying, _autoEvent, 0, 1000 * 60 * 5);
            using (var db = new NeoContext())
            {
                if (!db.Playings.Any()) return Task.CompletedTask;
                var game = db.Playings.OrderBy(o => Guid.NewGuid()).FirstOrDefault();
                _client.SetGameAsync(game.Name);
            }
            return Task.CompletedTask;
        }

        private void ChangePlaying(object stateInfo)
        {
            using (var db = new NeoContext())
            {
                if (!db.Playings.Any()) return;
                var game = db.Playings.OrderBy(o => Guid.NewGuid()).FirstOrDefault();
                _client.SetGameAsync(game.Name);
            }
        }

        private void RemindBdoBoss(object stateInfo)
        {
            DateTime dateNow = DateTime.Now;
            using (var db = new NeoContext())
            {
                foreach(var bossTime in BdoBossHelper.bossArray) //check all bosses
                {
                    if (bossTime.Item1 == dateNow.DayOfWeek)//if the boss is today 
                    { 
                        if((dateNow.Hour.ToString() + ":" + dateNow.Minute.ToString()) == bossTime.Item2) //hour matches
                        {
                            foreach (var item in db.BdoReminders)
                            {
                                var role = _client.GetGuild(item.Guild.Id).Roles.FirstOrDefault(x => x.Name == "BossTimer");
                                
                                (_client.GetChannel(item.ChannelID) as ITextChannel)?.SendMessageAsync($"15 minutes till {bossTime.Item3} {role?.Mention}");
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }                   
            }
        }

        private static Task Log(LogMessage l)
        {
            Task.Run(async ()
                => await NeoConsole.Log(l.Severity, l.Source, l.Exception?.ToString() ?? l.Message));
            return Task.CompletedTask;
        }

        private static async void EnsureConfigExists()
        {
            if (!Directory.Exists(Path.Combine(AppContext.BaseDirectory, "output")))
                Directory.CreateDirectory(Path.Combine(AppContext.BaseDirectory, "output"));
            if (!Directory.Exists(Path.Combine(AppContext.BaseDirectory, "data")))
                Directory.CreateDirectory(Path.Combine(AppContext.BaseDirectory, "data"));

            var loc = Path.Combine(AppContext.BaseDirectory, "data/configuration.json");

            if (!File.Exists(loc) || Configuration.Load().Token == null)
            {
                await NeoConsole.NewLine("Configuration not found...\nCreating...", ConsoleColor.Cyan);
                var config = new Configuration();
                await NeoConsole.NewLine("Token :", ConsoleColor.DarkCyan);
                config.Token = Console.ReadLine();
                await NeoConsole.NewLine("Default Prefix :", ConsoleColor.Cyan);
                config.Prefix = Console.ReadLine();
                await NeoConsole.NewLine("osu!api :", ConsoleColor.DarkCyan);
                config.OsuApiKey = Console.ReadLine();
                config.Save();
            }
            else
            {
                await NeoConsole.NewLine("Configuration found...\nLoading...", ConsoleColor.Cyan);
            }
        }
    }
}