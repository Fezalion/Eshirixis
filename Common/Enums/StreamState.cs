﻿
namespace Eshirixis.Common.Enums
{
    public enum StreamState
    {
        Resolving,
        Queued,
        Playing,
        Completed
    }
}
